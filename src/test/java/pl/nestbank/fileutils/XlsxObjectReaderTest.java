package pl.nestbank.fileutils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.nestbank.entity.Kid;

import java.io.IOException;
import java.util.List;

public class XlsxObjectReaderTest {

    private XlsxObjectReader xlsxObjectReader;

    @Before
    public void setup() {
        xlsxObjectReader = new XlsxKidReaderImpl();
    }

    @Test
    public void testReadObjectsFromFile() throws IOException {
        List<Kid> kids = xlsxObjectReader.readObjectsFromFile("dane.xlsx");
        kids.forEach(System.out::println);
        Assert.assertEquals(kids.size(), 6);
    }
}

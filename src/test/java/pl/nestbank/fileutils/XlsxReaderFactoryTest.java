package pl.nestbank.fileutils;

import org.junit.Before;
import org.junit.Test;
import pl.nestbank.entity.Kid;

import static org.junit.Assert.*;

public class XlsxReaderFactoryTest {

    private XlsxReaderFactory factory;

    @Before
    public void setup() {
        factory = new KidXlsxReaderFactory();
    }

    @Test
    public void testGetInstance() {
        XlsxObjectReader reader = factory.getInstance(Kid.class);
        assertNotNull(reader);
        assertTrue(reader instanceof XlsxObjectReader);
        assertTrue(reader instanceof XlsxKidReaderImpl);
    }
}

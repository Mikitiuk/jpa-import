package pl.nestbank.fileutils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.nestbank.dao.KidDao;
import pl.nestbank.entity.Kid;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class KidsImporterTest {

    private KidsImporter importer;
    private KidDao kidDao;

    @Before
    public void setup() {
        importer = new KidsImporter();
        kidDao = new KidDao();
    }

    @After
    public void teardown() {
        kidDao.deleteKid(1);
        kidDao.deleteKid(2);
        kidDao.deleteKid(3);
        kidDao.deleteKid(4);
        kidDao.deleteKid(5);
        kidDao.deleteKid(6);
    }

    @Test
    public void testImportKids() {
        String inputFile = "dane.xlsx";
        try {
            importer.importKids(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Kid> allKids = kidDao.readAllKids();
        assertEquals(allKids.size(), 6);
    }
}

package pl.nestbank.dao;

import org.junit.*;
import pl.nestbank.entity.Kid;

import java.util.List;

import static org.junit.Assert.*;

public class KidDaoTest {

    private KidDao kidDao;

    @Before
    public void setup() {
        kidDao = new KidDao();
    }

    @After
    public void teardown() {
        kidDao.close();
    }

    @Test
    public void testCreateKid() {
        Kid kid = new Kid(100, "test_name", "test_pet", 3);
        kidDao.createKid(kid);
        Kid savedKid = kidDao.readKidById(100);
        assertEquals(kid, savedKid);
        kidDao.deleteKid(savedKid.getId());
    }

    @Test
    public void testReadKidById() {
        Kid kid = new Kid(100, "test_name", "test_pet", 3);
        kidDao.createKid(kid);
        Kid savedKid = kidDao.readKidById(100);
        assertEquals(100, savedKid.getId());
        assertEquals("test_name", savedKid.getName());
        assertEquals("test_pet", savedKid.getAnimal());
        assertEquals(3, savedKid.getAge());
        kidDao.deleteKid(savedKid.getId());
    }

    @Test
    public void testReadAllKids() {
        List<Kid> allKids = kidDao.readAllKids();
        allKids.forEach(System.out::println);
        assertNotNull(allKids);
    }

    @Test
    public void testDeleteKid() {
        Kid kid = new Kid(100, "test_name", "test_pet", 3);
        kidDao.createKid(kid);
        Kid savedKid = kidDao.readKidById(kid.getId());
        kidDao.deleteKid(savedKid.getId());
        Kid shouldBeNull = kidDao.readKidById(100);
        System.out.println(shouldBeNull);
        assertNull(shouldBeNull);
    }

    @Test
    public void testUpdateKid() {
        Kid k1 = new Kid(100, "test_name", "test_pet", 3);
        Kid k2 = new Kid(100, "Mike", "Meow", 3);
        kidDao.createKid(k1);
        kidDao.updateKid(k2);
        Kid saved = kidDao.readKidById(100);
        assertEquals("Mike", saved.getName());
        assertEquals("Meow", saved.getAnimal());
        assertEquals(3, saved.getAge());
        kidDao.deleteKid(saved.getId());
    }
}

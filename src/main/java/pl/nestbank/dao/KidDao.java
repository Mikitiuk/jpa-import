package pl.nestbank.dao;

import pl.nestbank.entity.Kid;

import javax.persistence.*;
import java.util.List;

public class KidDao implements AutoCloseable {

    private EntityManagerFactory emf;
    private EntityManager entityManager;

    public KidDao() {
        emf = Persistence.createEntityManagerFactory("KidsUnit");
        entityManager = emf.createEntityManager();
    }

    public void createKid(Kid kid) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(kid);
        transaction.commit();
    }

    public Kid readKidById(int id) {
        return entityManager.find(Kid.class, id);
    }

    public List<Kid> readAllKids() {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Query query = entityManager.createQuery("SELECT k FROM Kid k");
        List<Kid> kidz = query.getResultList();
        transaction.commit();
        return kidz;
    }

    public void updateKid(Kid kid) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(kid);
        transaction.commit();
    }

    public void deleteKid(int id) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Kid toDelete = readKidById(id);
        entityManager.remove(toDelete);
        transaction.commit();
    }

    @Override
    public void close() throws PersistenceException {
        entityManager.close();
        emf.close();
    }
}

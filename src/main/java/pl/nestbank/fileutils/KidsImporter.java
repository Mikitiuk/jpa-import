package pl.nestbank.fileutils;

import pl.nestbank.dao.KidDao;
import pl.nestbank.entity.Kid;

import javax.persistence.PersistenceException;
import java.io.IOException;
import java.util.List;

public class KidsImporter {

    public void importKids(String inputFile) throws IOException {

        XlsxReaderFactory factory = new KidXlsxReaderFactory();
        XlsxObjectReader kidsReader;

        try(KidDao kidDao = new KidDao()) {
            kidsReader = factory.getInstance(Kid.class);
            List<Kid> kids = kidsReader.readObjectsFromFile(inputFile);
            kids.stream().forEach(kidDao::createKid);
        } catch (PersistenceException e) {
            System.err.println(">>>> ERROR: Make sure the file is valid and the " +
                    "database table does not already contain ids found in the sheet.");
        }
    }
}

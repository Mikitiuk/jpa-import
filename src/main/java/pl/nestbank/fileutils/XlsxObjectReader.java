package pl.nestbank.fileutils;

import java.io.IOException;
import java.util.List;

public interface XlsxObjectReader<T> {

    List<T> readObjectsFromFile(String filepath) throws IOException;
}

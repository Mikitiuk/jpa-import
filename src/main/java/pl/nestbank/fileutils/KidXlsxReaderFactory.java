package pl.nestbank.fileutils;

import pl.nestbank.entity.Kid;

public class KidXlsxReaderFactory extends XlsxReaderFactory {

    @Override
    public XlsxObjectReader getInstance(Class c) {
        if (c == Kid.class)
            return new XlsxKidReaderImpl();
        else {
            String className = c.getClass().getSimpleName();
            throw new UnsupportedOperationException(
                    "Class " + className + " not supported");
        }
    }
}

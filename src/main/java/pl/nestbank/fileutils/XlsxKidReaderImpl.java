package pl.nestbank.fileutils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import pl.nestbank.entity.Kid;

public class XlsxKidReaderImpl implements XlsxObjectReader, Closeable {

    @Override
    public List<Kid> readObjectsFromFile(String filePath) throws IOException {

        List<Kid> kids = new ArrayList<>();
        XSSFSheet sheet = getSpreadSheet(filePath);
        int numberOfObjects = getLastColumnIndex(sheet);
        for (int i = 1; i <= numberOfObjects; i++) {
            Kid kid = getObjectFromColumn(sheet, i);
            kids.add(kid);
        }
        return kids;
    }

    private XSSFSheet getSpreadSheet(String filePath) throws IOException {
        File file = new File(filePath);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        return workbook.getSheetAt(0);
    }

    private Kid getObjectFromColumn(XSSFSheet sheet, int columnIndex) {
        Kid kid = new Kid();
        for (Row row : sheet) {
            Cell cell = row.getCell(columnIndex);
            if (cell != null) {
                int rowIndex = row.getRowNum();
                switch (rowIndex) {
                    case 0: kid.setId((int) cell.getNumericCellValue());
                        break;
                    case 1: kid.setName(cell.getStringCellValue());
                        break;
                    case 2: kid.setAnimal(cell.getStringCellValue());
                        break;
                    case 3: kid.setAge((int) cell.getNumericCellValue());
                        break;
                }
            }
        }
        return kid;
    }

    private int getLastColumnIndex(XSSFSheet sheet) {
        int index = 0;
        Iterator<Cell> cellIterator = sheet.getRow(0).cellIterator();
        while (cellIterator.hasNext()) index = cellIterator.next().getColumnIndex();
        return index;
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException();
    }
}

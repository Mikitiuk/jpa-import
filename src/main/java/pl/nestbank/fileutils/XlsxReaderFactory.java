package pl.nestbank.fileutils;

public abstract class XlsxReaderFactory {

    public abstract XlsxObjectReader getInstance(Class c);
}

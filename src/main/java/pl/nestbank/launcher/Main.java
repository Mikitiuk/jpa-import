package pl.nestbank.launcher;

import pl.nestbank.fileutils.KidsImporter;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        String inputFilePath;
        String logFileName = "jpa-import-tool.log";

        if (args.length != 1) {
            printUsage();
            System.exit(1);
        } else {
            inputFilePath = args[0];
            KidsImporter importer = new KidsImporter();
            System.out.println("Check the log file for results.");
            try {
                System.setOut(outputFile(logFileName));
                System.setErr(outputFile(logFileName));
                importer.importKids(inputFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void printUsage() {
        System.err.println("USAGE: java -jar jpa-import-tool.jar <path to proper xlsx file>");
    }

    private static PrintStream outputFile(String name) throws FileNotFoundException {
        return new PrintStream(new BufferedOutputStream(new FileOutputStream(name)), true);
    }
}
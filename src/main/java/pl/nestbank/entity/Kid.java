package pl.nestbank.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "kids")
public class Kid {

    @Id
    private int id;
    private String name;
    private String animal;
    private int age;
}

# JPA spreadsheet import tool

A project made for recruitment purposes.

### Getting Started
- make sure you have git and maven 3 installed on your machine
- check out the project & cd to its directory
- launch `mvn package` maven command to generate a jar file
- then use `java` command specifying a path to a valid XLSX file  
`java -jar target/jpa-import-tool-jar-with-dependencies.jar <path to proper xlsx file>`
- check the logs for details  
`cat jpa-import-tool.log`
